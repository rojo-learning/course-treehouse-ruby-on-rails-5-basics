
### Using the Rails Console

The Rails Console is a tool that allows working with models trough the terminal using Ruby instructions.

```bash
  bin/rails console
```

# Reading Model Objects

Inside the Rails console, all the models defined in the application are available as classes. Suppose there is a posts table, then the records can be manipulated as instances of the Post class.

```ruby
  # Get a post by id
  post = Post.find(post_id)
  # Get the first created post
  post = Post.first
  # Get the most recent post
  post = Post.last
  # Get a post with a specific attribute value. (Returns the first post found)
  post = Post.find_by(tag: 'adventures')
  # Get posts with a specific attribute value. (Returns all the posts found)
  post = Post.where(tag: 'adventures')
  # Get all posts
  posts = Post.all
```

# Creating Model Objects

```ruby
  # Instance a new Post
  post = Post.new
  # Set the post attributes
  post.title = 'Some title'
  post.tag = 'some-tag'
  post.content = 'Some content'
  # Store the post into the database
  post.save
```

# Updating Model Objects

```ruby
  # Get the desired post
  post = Post.find(post_id)
  # Update the desired values
  post.update_attributes(tag: 'vacation')
```

# Deleting Model Objects

```ruby
  # Get the desired post
  post = Post.find(post_id)
  # Delete its information from the database
  post.destroy
```
