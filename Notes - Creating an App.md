
### Creating an App

# The MVC Pattern

One of the conventions Rails follows is a pattern that's widely used in software development, called "Model, View, Controller", or MVC.

* The models write Ruby objects to the database, and read them out again later.
* The views show data to users, most often in the form of HTML web pages.
* Controllers respond to requests from users, usually by coordinating the model and the view.


# Rails `new` Application

```bash
  # create the skeleton of a new Rails application
  rails new [name_of_your_app]
  # go into the folder of the new app
  cd name_of_your_app
  # start the application service
  # (visit http://localhost:3000/ on the browser)
  bin/rails server
```

# Creating Resources with Scaffolds

```bash
  bin/rails generate scaffold ModelName [attribute_1:type, ...]
```

This generates the necessary migration,routes, controller (with actions) and views to perform the basic CRUD actions on the defined model.

* `rails` runs the Rails command
* `generate` subcommand will create some source code files for us.
* `scaffold` is the type of code we want to generate.
* `ModelName` is the model we want to create a scaffold for.
* Then, we add one or more attributes that we want our model objects to have. These are bits of data that describe the object.

# Applying Migrations to the Schema

Migrations are files that specify changes to the schema. These files have descriptive names with timestamps of when the changes where defined, so they are sort by date and can be applied gradually or even rolled back.

The schema keeps track of the latest migration applied, so if a newer migration is defined but not applied, Rails will complain with an `ActiveRecord::PendingMigrationError`.

As a scaffold generation creates a migration, it must be applied to the schema as follows:

```bash
  bin/rails db:migrate
```

To see new resources created via scaffold start the Rails service and go to http://localhost:3000/[pluratized_model_name]
