
# Treehouse - Ruby on Rails 5 Basics #

This repository contains notes and practice examples from **Ruby on Rails 5 Basics**, imparted by Jay McGavren at [Threehouse][JF].

> We're going to set up a simple blogging app with just a handful of Rails commands. Then, we'll dive into what we've created and help you really understand what's going on. We'll show you how to work with models, views, and controllers to quickly add new features to your app. By the end of this course, you'll be ready to create basic web apps in Rails.

## Contents ##

- **Creating an App**: We'll create a new Rails app, and a simple resource. A resource is something you want users to be able to create, read, update, and delete instances of. With just a couple Rails commands, your site will be ready to run and fully interactive!

- **Handling Requests**: In this stage, we'll take a closer look at the resource we've created, and show you how Rails handles requests.

- **Using the Rails Console**: The Rails console is useful when you need to do an operation on many model objects at once, or to look at model attributes that you haven't yet added to your views. We'll show you how to update model objects using the console.

- **Adding a Model Attribute**: Our app is able to store titles for our posts, but we forgot to add a post body to hold the actual post content. In this stage, we're going to fix that. First, we need to add a column to the database, so that our post model objects can store the value of their body attributes. Then we'll need to update our views to let us show those values. And finally, we'll need to update the controller to allow new body text to be submitted from HTML forms.


---
This repository contains code examples from Treehouse. These are included under fair use for showcasing purposes only. Those examples may have been modified to fit my particular coding style.

[JF]: http://teamtreehouse.com
